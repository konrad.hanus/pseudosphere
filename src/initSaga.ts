import * as sagas from './rootSaga';
import { SagaMiddleware } from 'redux-saga';

export default function initSagas(sagaMiddleware: SagaMiddleware<{}>) {
  Object.keys(sagas).map(key => sagas[key]).forEach(sagaMiddleware.run);
}
