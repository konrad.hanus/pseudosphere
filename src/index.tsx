import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import store, {sagaMiddleware} from './store';
import initSagas from './initSaga';
import RootRouter from './rootRouter';
initSagas(sagaMiddleware);

ReactDOM.render(
  <Provider store={store}>
    <RootRouter />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
