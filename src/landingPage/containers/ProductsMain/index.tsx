
import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../../rootType';
import ListView from '../../components/ListView';
import { actionCreators } from './action';
import { Product, SelectedProduct } from '../../../rootType';


export interface ProductsMainProps {
  allProducts: Product[];
  searchFieldInput: any;
  selectedProduct: SelectedProduct;
}

type props = ProductsMainProps & typeof actionCreators;
class ProductsMain extends React.Component<props> {

    public render() {
        console.log(this.props.allProducts);
        return (
            <React.Fragment>
                <ListView allProducts={this.props.allProducts} selectedProduct={this.props.selectedProduct} selectProduct={this.props.selectProduct}/> 
            </React.Fragment>
        );
    }
}

export default connect(
  (state: ApplicationState): ProductsMainProps => {
    return {
      allProducts: state.allProducts,
      searchFieldInput: state.searchFieldInput,
      selectedProduct: state.selectedProduct
    };
  },
  actionCreators
)(ProductsMain);
