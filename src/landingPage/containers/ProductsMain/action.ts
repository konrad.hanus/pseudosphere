import {Product} from '../../../rootType';

export const CHANGE_SEARCH_FIELD_INPUT = 'CHANGE_SEARCH_FILED_INPUT';
export type CHANGE_SEARCH_FIELD_INPUT = typeof CHANGE_SEARCH_FIELD_INPUT;
export interface ChangeSearchFieldInputAction { type: CHANGE_SEARCH_FIELD_INPUT; payload: string; }

export const SELECT_PRODUCT = 'SELECT_PRODUCT';
export type SELECT_PRODUCT = typeof SELECT_PRODUCT;
export interface SelectProductAction { type: SELECT_PRODUCT; payload: Product; }


export type ProdcutsAction = ChangeSearchFieldInputAction | SelectProductAction;

export const actionCreators = {
    changeSearchFieldInput: (keywords: string) => <ChangeSearchFieldInputAction> {type: CHANGE_SEARCH_FIELD_INPUT, payload: keywords},
    selectProduct: (product: Product) => <SelectProductAction> { type: SELECT_PRODUCT, payload: product }
}