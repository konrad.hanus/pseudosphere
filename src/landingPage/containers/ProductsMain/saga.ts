
import { put, takeEvery } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import {
    SelectProductAction,
    actionCreators, 
    SELECT_PRODUCT
} from './action';


export function* onTestSaga(action: SelectProductAction): SagaIterator {

    console.log('form saga');
    console.log(action.payload.name);

   yield put(actionCreators.changeSearchFieldInput(action.payload.name));
}


export function* testSaga(): SagaIterator {
    yield takeEvery(SELECT_PRODUCT, onTestSaga);
}
