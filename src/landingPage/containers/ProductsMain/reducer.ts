import { Reducer } from 'redux';
import { SelectProductAction, SELECT_PRODUCT, ChangeSearchFieldInputAction, CHANGE_SEARCH_FIELD_INPUT } from './action';
import { Product, SelectedProduct, SearchFieldInput } from '../../../rootType';

const initialSelectedProduct = <SelectedProduct>{ product: { id: 1, name: 'test2', price: 11 } };
const allProducts = <Product[]>[{ id: 1, name: 'test2', price: 11 }];
const initialSearchFieldInput = <SearchFieldInput>{ value: 'test' };

export const selectedProductReducer: Reducer<SelectedProduct> =
    (state: SelectedProduct = initialSelectedProduct, action: SelectProductAction) => {
        switch (action.type) {
            case SELECT_PRODUCT:
                console.log("reducer: SELECT_PRODUCT", state, action);
                return { ...state, selected: action.payload }
            default: {
                return { ...state }
            }
        }
    }

export const allProductsReducer: Reducer<Product[]> =
    (state: Product[] = allProducts) => {
        return { ...state, allProducts: [{ id: 1, name: 'TV', price: 10 }, { id: 2, name: 'Monitor', price: 110 }, { id: 3, name: 'Computer', price: 101 }] }
    }

export const searchFiledInputReducer: Reducer<SearchFieldInput> =
    (state: SearchFieldInput = initialSearchFieldInput, action: ChangeSearchFieldInputAction) => {
        switch (action.type) {
            case CHANGE_SEARCH_FIELD_INPUT:
                console.log("reducer: CHANGE_SEARCH_FIELD_INPUT", state, action);
                return { ...state, searchFieldInput: action.payload }
            default: {
                return { ...state }
            }
        }
    }

