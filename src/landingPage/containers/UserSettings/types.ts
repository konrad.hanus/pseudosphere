export interface UserValues{
    userId: string,
    backgroundColor: string,
    showSometing: boolean,
    showSomethingElse: boolean,
    selectSetting: string
}

export const enum UserSettingsActionTypes{
    FETCH_USERSETTINGS = '@@UserSetttings/FETCH_USERSETTINGS',
    FETCH_USERSETTINGS_REJECTED ='@@UserSetttings/FETCH_USERSETTINGS_REJECTED',
    FETCH_USERSETTINGS_FULFILLED ='@@UserSetttings/FETCH_USERSETTINGS_FULFILLED',
    UPDATE_USERSETTINGS ='@@UserSetttings/UPDATE_USERSETTINGS',
}

export interface UserSettingsState {
    readonly error: string,
    readonly userValues: UserValues,
    readonly fetched: boolean,
    readonly fetching: boolean
}