import {action} from 'typesafe-actions';
import {
    UserSettingsActionTypes,
    UserValues
  } from "./types";

  export const actionCreators = {
    fetchError: (message:string) =>action(UserSettingsActionTypes.FETCH_USERSETTINGS_REJECTED,message),
    fetchSuccess:(data: UserValues) => action(UserSettingsActionTypes.FETCH_USERSETTINGS_FULFILLED, data),
    fetchUserSettings:()=>action(UserSettingsActionTypes.FETCH_USERSETTINGS),
 

  }