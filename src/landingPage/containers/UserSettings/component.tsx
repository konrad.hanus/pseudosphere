import * as React from "react";
 import "../../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { connect } from 'react-redux';
import { actionCreators } from './action';
import {
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  Col,
  Label,
  Input,
  CardFooter,
  Button,
  Row
} from "reactstrap";
import { UserValues } from "./types";
import { ApplicationState } from "../../../rootType";

export interface UserSettingsProps{
  userValues: UserValues
}
type props = UserSettingsProps & typeof actionCreators;
export class UserSettingsComponent extends React.Component<props> {
  public componentDidMount(){
    this.props.fetchUserSettings();
  }
  public handleOptionChange(changeEvent:any) {
    this.setState({
      selectedOption: changeEvent.target.value
    });
  }

  public render() {
    const userValues = this.props.userValues;
    return (
      <Row>
          <Col xs="12" md="12">
          <Card>
            <CardHeader>
              <strong>User Settings</strong>
            </CardHeader>
            <CardBody>
              <Form
                action=""
                method="post"
                encType="multipart/form-data"
                className="form-horizontal"
              >
                <FormGroup row={true}>
                  <Col md="3">
                    <Label>User id</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <p className="form-control-static">{userValues.userId}</p>
                  </Col>
                </FormGroup>
                <FormGroup row={true}>
                  <Col md="3">
                    <Label htmlFor="select">Select Something</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input type="select" name="select" id="select">
                      <option value="0">Please select</option>
                      <option value="1">Option #1</option>
                      <option value="2">Option #2</option>
                      <option value="3">Option #3</option>
                    </Input>
                  </Col>
                </FormGroup>

                <FormGroup row={true}>
                  <Col md="3">
                    <Label htmlFor="selectSm">Select Small</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="select"
                      name="selectSm"
                      id="SelectLm"
                      bsSize="sm"
                    >
                      <option value="0">Please select</option>
                      <option value="1">Option #1</option>
                      <option value="2">Option #2</option>
                      <option value="3">Option #3</option>
                      <option value="4">Option #4</option>
                      <option value="5">Option #5</option>
                    </Input>
                  </Col>
                </FormGroup>

                <FormGroup row={true}>
                  <Col md="3">
                    <Label>Show something</Label>
                  </Col>
                  <Col md="9">
                    <FormGroup check={true} className="radio">
                      <Input
                        className="form-check-input"
                        type="radio"
                        id="radio1"
                        name="radios"
                        value="option1"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="radio1"
                      >
                        Option 1
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} className="radio">
                      <Input
                        className="form-check-input"
                        type="radio"
                        id="radio2"
                        name="radios"
                        value="option2"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="radio2"
                      >
                        Option 2
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} className="radio">
                      <Input
                        className="form-check-input"
                        type="radio"
                        id="radio3"
                        name="radios"
                        value="option3"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="radio3"
                      >
                        Option 3
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row={true}>
                  <Col md="3">
                    <Label>Show something else</Label>
                  </Col>
                  <Col md="9">
                    <FormGroup check={true} inline={true}>
                      <Input
                        className="form-check-input"
                        type="radio"
                        id="inline-radio1"
                        name="inline-radios"
                        value="true"
                        checked={userValues.showSomethingElse}
                        // tslint:disable-next-line:jsx-no-bind
                        onChange={this.handleOptionChange.bind(this)}
                      />
                      <Label
                        className="form-check-label"
                        check={true}
                        htmlFor="inline-radio1"
                      >
                        True
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} inline={true}>
                      <Input
                        className="form-check-input"
                        type="radio"
                        id="inline-radio2"
                        name="inline-radios"
                        value="false"
                        // checked={!userValues.showSomethingElse}
                        // tslint:disable-next-line:jsx-no-bind
                        onChange={this.handleOptionChange.bind(this)}
                      />
                      <Label
                        className="form-check-label"
                        check={true}
                        htmlFor="inline-radio2"
                      >
                        False
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row={true}>
                  <Col md="3">
                    <Label>Checkboxes</Label>
                  </Col>
                  <Col md="9">
                    <FormGroup check={true} className="checkbox">
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="checkbox1"
                        name="checkbox1"
                        value="option1"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="checkbox1"
                      >
                        Option 1
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} className="checkbox">
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="checkbox2"
                        name="checkbox2"
                        value="option2"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="checkbox2"
                      >
                        Option 2
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} className="checkbox">
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="checkbox3"
                        name="checkbox3"
                        value="option3"
                      />
                      <Label
                        check={true}
                        className="form-check-label"
                        htmlFor="checkbox3"
                      >
                        Option 3
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row={true}>
                  <Col md="3">
                    <Label>Inline Checkboxes</Label>
                  </Col>
                  <Col md="9">
                    <FormGroup check={true} inline={true}>
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="inline-checkbox1"
                        name="inline-checkbox1"
                        value="option1"
                      />
                      <Label
                        className="form-check-label"
                        check={true}
                        htmlFor="inline-checkbox1"
                      >
                        One
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} inline={true}>
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="inline-checkbox2"
                        name="inline-checkbox2"
                        value="option2"
                      />
                      <Label
                        className="form-check-label"
                        check={true}
                        htmlFor="inline-checkbox2"
                      >
                        Two
                      </Label>
                    </FormGroup>
                    <FormGroup check={true} inline={true}>
                      <Input
                        className="form-check-input"
                        type="checkbox"
                        id="inline-checkbox3"
                        name="inline-checkbox3"
                        value="option3"
                      />
                      <Label
                        className="form-check-label"
                        check={true}
                        htmlFor="inline-checkbox3"
                      >
                        Three
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
              </Form>
            </CardBody>
            <CardFooter>
              <Button type="submit" size="sm" color="primary">
                <i className="fa fa-dot-circle-o" /> Submit
              </Button>
              <Button type="reset" size="sm" color="danger">
                <i className="fa fa-ban" />
                Reset
              </Button>
            </CardFooter>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default connect(
  ((state: ApplicationState): UserSettingsProps => {
    return ({
      userValues: state.userSettings.userValues
    });
  }), 
  actionCreators
)(UserSettingsComponent);

