import { all, call,apply, fork, put, takeEvery } from "redux-saga/effects";
import { actionCreators } from "./action";
import { UserSettingsActionTypes } from "./types";
// import { UserSettings} from "./userSettings.types";

function* handleFetch(action: any) {
  const userId = "f28f6251-ac75-4c75-1ed1-e4bbbb33bf8f";
  const url = `/api/getUserSettings/${userId}`;
  const response = yield call(fetch, url);

  if (response.ok) {
    const userSettingsFromDatabase = yield apply(response, response.json);
    // TODO discuss with a team if we would like to data conversion here or asign all data fromdb
    // const userSettings:UserSettings={
    //   backgroundColor:userSettingsFromDatabase[0].backgroundColor,
    //   selectSetting:userSettingsFromDatabase[0].selectSetting,
    //   showSomethingElse:userSettingsFromDatabase[0].showSomethingElse,
    //   showSometing:userSettingsFromDatabase[0].showSometing,
    //   userId:userSettingsFromDatabase[0].userId
    // }
    yield put(actionCreators.fetchSuccess(userSettingsFromDatabase[0]));
  } else {
    yield put(actionCreators.fetchError(response.error));
  }
}

// This is our watcher function. We use `take*()` functions to watch Redux for a specific action
// type, and run our saga, for example the `handleFetch()` saga above.
function* watchFetchRequest() {
  yield takeEvery(UserSettingsActionTypes.FETCH_USERSETTINGS, handleFetch);
}

// We can also use `fork()` here to split our saga into multiple watchers.
export function* userSettingsSaga() {
  yield all([fork(watchFetchRequest)]);
}

export default userSettingsSaga;
