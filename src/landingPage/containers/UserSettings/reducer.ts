import { Reducer } from "redux";
import {
    UserValues,
  UserSettingsActionTypes,
  UserSettingsState
} from "./types";

const userValues: UserValues={
    backgroundColor:"",
    selectSetting:"",
    showSomethingElse:false,
    showSometing:false,
    userId:""
}

const initialState: UserSettingsState = {
  error: "",
  fetched: false,
  fetching: false,
  userValues
};
const reducer: Reducer<UserSettingsState> = (state = initialState, action) => {
  switch (action.type) {
    case UserSettingsActionTypes.FETCH_USERSETTINGS: {
      return { ...state, fetching: true };
    }
    case UserSettingsActionTypes.FETCH_USERSETTINGS_REJECTED: {
      return { ...state, fetching: false, error: action.payload };
    }
    case UserSettingsActionTypes.FETCH_USERSETTINGS_FULFILLED: {
      return {
        error: "",
        fetched: true,
        fetching: false,
        userValues: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export { reducer as userSettingsReducer };
