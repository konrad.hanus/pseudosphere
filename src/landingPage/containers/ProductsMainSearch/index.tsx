import * as React from 'react';
import { connect } from 'react-redux';
import SearchField from '../../components/SearchField';
import { actionCreators } from '../ProductsMain/action';
import { Product, SelectedProduct, ApplicationState } from '../../../rootType';

export interface ProductsMainProps {
    allProducts: Product[];
    searchFieldInput: any,
    selectedProduct: SelectedProduct
}

type props = ProductsMainProps & typeof actionCreators;
class ProductsMainSearch extends React.Component<props> {
    public render() {
        console.log(this.props.allProducts);
        return (
            <React.Fragment>
                <SearchField searchFieldInput={this.props.searchFieldInput} changeSearchFiledInput={this.props.changeSearchFieldInput}/>
            </React.Fragment>
        );
    }
}

export default connect(
    ((state: ApplicationState): ProductsMainProps => {
      return ({
        allProducts: state.allProducts,
        searchFieldInput: state.searchFieldInput,
        selectedProduct: state.selectedProduct
      });
    }), 
    actionCreators
  )(ProductsMainSearch);
  