import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../../../rootType';
import ProductsDetailItem from '../../components/ProductDetailItem';

export interface ProductsDetailProps {
  selectedProduct: any
}
type props = ProductsDetailProps;

export class ProductsDetail extends React.Component<props> {
  public render() {
    return (
      <div>
        <ProductsDetailItem selectedProduct={this.props.selectedProduct} />
      </div>
    );
  }
}

export default connect(
  ((state: ApplicationState): ProductsDetailProps => {
    return ({
      selectedProduct: state.selectedProduct
    });
  })
)(ProductsDetail);
