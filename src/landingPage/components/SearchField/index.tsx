import * as React from 'react';

export interface SearchFieldProps {
  searchFieldInput: any,
  changeSearchFiledInput: any
}

type props = SearchFieldProps;

class SearchField extends React.Component<props> {
  public render() {
    return (
      <div>
        <input value={this.props.searchFieldInput.searchFieldInput} onChange={(e) => this.props.changeSearchFiledInput(e.target.value)} className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
        <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        <br />
        Search: {this.props.searchFieldInput.searchFieldInput}  
      </div>
    );
  }
}

export default SearchField;
