import * as React from "react";

import ProductMain from "../../containers/ProductsMain";
class Content extends React.Component {
  public render() {
    return (
      <div className="col-xl-10 col-md-9">
        <div className="row justify-content-center pt-5">
          <ProductMain />
        </div>
      </div>
    );
  }
}


export default Content;
