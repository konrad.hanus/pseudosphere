import * as React from 'react';
import { Link } from 'react-router-dom';
const Urls = () => (
    <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
            <Link to="/">
                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
            </Link>
        </li>
        <li className="nav-item">
            <Link to="/login">
                <a className="nav-link" href="#">Login</a>
            </Link>
        </li>
        <li className="nav-item">
            <Link to="/userSettings">
                <a className="nav-link" href="#">User Settings</a>
            </Link>
        </li>
        <li className="nav-item">
            <a className="nav-link disabled" href="#">Disabled</a>
        </li>
    </ul>
);

export default Urls;
