import * as React from 'react';
import ListViewItem from '../ListViewItem';
import { SelectedProduct } from '../../../rootType';

export interface ListViewProps {
  allProducts: any;
  selectedProduct: SelectedProduct;
  selectProduct: any;
}

type props = ListViewProps;

class ListView extends React.Component<props> {
  public render() {
    const allProducts = this.props.allProducts.allProducts;
    return (
      <React.Fragment>
        {allProducts.map((product:any, key: number) =>
           <ListViewItem key={key} selectedProduct={this.props.selectedProduct} selectProduct={this.props.selectProduct} product={product} />
        )} 
      </React.Fragment>
    );
  }
}

export default ListView;
