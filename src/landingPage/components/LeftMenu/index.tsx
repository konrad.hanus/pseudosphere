import * as React from 'react';
import ExpressServerSample from '../ExpressSeverSample';

const LeftMenu = () => (
      <aside className="col-xl-2 col-md-3 pt-5 sp-container sp-filter">
        <div className="form-group mt-5">
          <ExpressServerSample />
          <label htmlFor="Lorem1">Lorem ipsum</label>
          <input type="text" className="form-control" id="Lorem1" />
        </div>
        <div className="form-group">
          <label htmlFor="Lorem2">Select list:</label>
          <select className="form-control" id="Lorem2">
            <option>1</option>
            <option>2</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="Lorem3">Select list:</label>
          <select className="form-control" id="Lorem3">
            <option>1</option>
            <option>2</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="formControlRange1">Example Range input</label>
          <input type="range" className="form-control-range" id="formControlRange1" />
        </div>
        <div className="form-group">
          <label htmlFor="formControlRange2">Example Range input</label>
          <input type="range" className="form-control-range" id="formControlRange2" />
        </div>
        <div className="mt-5">
          <div className="form-group">
            <label htmlFor="SortBy1">Example select</label>
            <select className="form-control" id="SortBy1">
              <option>Sort1</option>
              <option>Sort2</option>
            </select>
          </div>
          <div className="form-group">
            <select className="form-control" id="SortBy2">
              <option>Sort1</option>
              <option>Sort2</option>
            </select>
          </div>
        </div>
      </aside>

    );

export default LeftMenu;
