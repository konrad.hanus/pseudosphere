import * as React from 'react';

import ProductDetail from '../../containers/ProductDetail';

const Product = () => (
    <div className="card bg-light sp-product m-5">
        <div className="card-header">
            <button type="button" className="btn btn-sm btn-primary pull-right">Button</button>
        </div>
        <div className="card-body">


            <ProductDetail />

            <p className="font-weight-normal text-center">Product name</p>
            <img src="http://place-hold.it/100x100" className="rounded mx-auto d-block mb-1" alt="Cinque Terre" />
            <span className="badge badge-pill badge-primary sp-product-badge mx-auto d-block">Badge 1</span>
            <span className="badge badge-pill badge-primary sp-product-badge mx-auto d-block">Badge 2</span>
        </div>
        <div className="card-footer container">
            <div className="row">
                <div className="col-sm">
                    <p className="font-weight-light sp-product-desc mb-0">Light weight text.</p>
                    <p className="font-weight-light sp-product-desc mb-0">0%</p>
                </div>
                <div className="col-sm">
                    <p className="font-weight-light sp-product-desc mb-0">Light weight text.</p>
                    <p className="font-weight-light sp-product-desc mb-0">0%</p>
                </div>
                <div className="col-sm">
                    <p className="font-weight-light sp-product-desc mb-0">Light weight text.</p>
                    <p className="font-weight-light sp-product-desc mb-0">0%</p>
                </div>
                <div className="col-sm">
                    <p className="font-weight-light sp-product-desc mb-0">Light weight text.</p>
                    <p className="font-weight-light sp-product-desc mb-0">0%</p>
                </div>
            </div>
        </div>
    </div>

);

export default Product;
