import * as React from 'react';
import Menu from '../Menu'

const Header = () => (
      <nav className="navbar navbar-expand-md navbar-light fixed-top bg-info">
        <a className="navbar-brand" href="#">Fixed navbar</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarCollapse">
          <Menu />
        </div>
      </nav>
    );

export default Header;
