import * as React from 'react';
import ProductsMainSearch from '../../containers/ProductsMainSearch';

const Search = () => (
        <form className="form-inline mt-2 mt-md-0">
        <ProductsMainSearch />
        </form>
    );
export default Search;
                