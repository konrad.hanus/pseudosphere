import * as React from 'react';
import Urls from '../Urls';
import Search from '../Search';

const Menu = () => (
        <React.Fragment>
            <Urls />
            <Search />
        </React.Fragment>
    );

export default Menu;
