import * as React from "react";

export class ExpressServerSample extends React.PureComponent {
  public state = {
    response: ""
  };

  public componentDidMount() {
    // this.callApi()
    //   .then(res => this.setState({ response: res }))
    //   // tslint:disable-next-line:no-console
    //   .catch(err => console.log(err));
  }

  public callApi = async () => {
    const response = await fetch("/api/getUserSettings");
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message);
    }

    return JSON.stringify(body);
  };
  public render() {
    return (
      <div>
          <p>Data from Azure CosmosDB with MongoApi</p>
        <p className="App-intro">{this.state.response}</p>
      </div>
    );
  }
}

export default ExpressServerSample;
