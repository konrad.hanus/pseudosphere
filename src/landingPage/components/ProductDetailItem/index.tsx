import * as React from 'react';


export interface ProductsDetailItemProps {
  selectedProduct: any
}
type props = ProductsDetailItemProps;

class ProductsDetailItem extends React.Component<props> {

  public isSetProduct() {
    for (const key in this.props.selectedProduct) {
      if (this.props.selectedProduct.hasOwnProperty(key)) {
        return true;
      }
    }
    return false;
  }
  public render() {
   

    console.log(this.isSetProduct());
    return (
      <div>
      {this.isSetProduct() ? <b>{this.props.selectedProduct.selected.name}<br />{this.props.selectedProduct.selected.price} PLN</b> : 'product no selected'}
      </div>
    );
  }
}

export default ProductsDetailItem;
