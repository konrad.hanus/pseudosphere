import * as React from 'react';
import Content from '../Content';
import LeftMenu from '../LeftMenu';

const Container = () => (
    <main role="main" className="container-fluid sp-container">
        <div className="row sp-container">
            <LeftMenu />
            <Content />
        </div>
    </main>
);

export default Container;
