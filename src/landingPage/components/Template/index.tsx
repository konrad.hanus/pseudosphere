import * as React from 'react';

import Header from '../Header';
import Container from '../Contener';
import Footer from '../Footer';
import './style.css';
const Template = () => (
      <React.Fragment>
        <Header />
        <Container />
        <Footer />
      </React.Fragment>
    );

export default Template;
