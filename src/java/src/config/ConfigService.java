package config;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import config.mongo.MongoCustomClient;

@Path("/")
public class ConfigService {
	
	@GET
	@Path("/health")
	@Produces(MediaType.APPLICATION_JSON)
	public Response health() {
		MongoCustomClient db = new MongoCustomClient();
		
		String result = "{\"endpoint\": \"ok\",\"productsCount\": \""+db.countProducts()+"\"}";
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/productByName")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductByName(@QueryParam("productName") String productName) {
		String result = new MongoCustomClient().getProductByKey("productName", productName);
		return Response.status(200).entity(result).build();
	} 
	
	
	@GET
	@Path("/productByCode")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductByCode(@QueryParam("productCode") String productCode) {
		String result = new MongoCustomClient().getProductByKey("productCode", productCode);
		return Response.status(200).entity(result).build();
	} 
	

}
