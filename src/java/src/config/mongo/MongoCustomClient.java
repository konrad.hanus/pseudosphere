package config.mongo;

import org.bson.Document;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoClient;
import static com.mongodb.client.model.Filters.*;


public class MongoCustomClient {
	private MongoClient mongoClient;
	private MongoDatabase database;
	
	public MongoCustomClient() {

		mongoClient = MongoClients.create("mongodb://localhost:27017");
		database = mongoClient.getDatabase("ConfigServiceDb");
	}
	
	public String getProduct(String productName) {
		String jsonRsp= "{mongoStatus: 'nok'}";
		
		MongoCollection<Document> collection = database.getCollection("products");
		jsonRsp=collection.find(eq("productName", productName)).first().toJson();
		mongoClient.close();
	
		return jsonRsp;

	}
	
	
	public String getProductByKey(String key, String value) {
		String jsonRsp= "{mongoStatus: 'nok'}";
	
		try {
		MongoCollection<Document> collection = database.getCollection("products");
		jsonRsp = collection.find(eq(key, value)).first().toJson();
		mongoClient.close();
		}
		catch(MongoSocketOpenException | MongoTimeoutException me) {
			
		return jsonRsp;
		}
		return jsonRsp;

	}
	
	public int countProducts() {
		int count=-1;
		try {
			MongoCollection<Document> collection = database.getCollection("products");
			count = (int) collection.countDocuments();
		}
		catch(MongoSocketOpenException | MongoTimeoutException me) {
			return -1;
		}
		return count;
	}

}
