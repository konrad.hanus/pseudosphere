
import * as React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Template from "./landingPage/components/Template";
import UserSettings from "./landingPage/containers/UserSettings/component";
class RootRouter extends React.Component {
    public render() {

        return (
          <Router>
            <React.Fragment>
              <Route exact={true} path="/" component={Template} />
              <Route path="/login" component={Template} />
              <Route path="/userSettings" component = {UserSettings}/>
            </React.Fragment>
          </Router>
        );
    }
}


export default RootRouter;
