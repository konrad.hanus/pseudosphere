import { combineReducers } from 'redux';
import { selectedProductReducer, allProductsReducer, searchFiledInputReducer } from './landingPage/containers/ProductsMain/reducer'
import { userSettingsReducer } from './landingPage/containers/UserSettings/reducer';


const rootReducer = combineReducers({
    allProducts: allProductsReducer,
    searchFieldInput: searchFiledInputReducer,
    selectedProduct: selectedProductReducer,
    userSettings: userSettingsReducer
});

export default rootReducer;
