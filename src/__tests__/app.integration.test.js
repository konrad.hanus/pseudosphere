const request = require("supertest");
const app = require("../server/app");
const UserSettings = require("../server/userSettingsModel");

function S4() {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

function guid() {
  // then to call it, plus stitch in '4' in the third group
  return (
    S4() +
    S4() +
    "-" +
    S4() +
    "-4" +
    S4().substr(0, 3) +
    "-" +
    S4() +
    "-" +
    S4() +
    S4() +
    S4()
  ).toLowerCase();
}

describe("Test the get user settings", () => {
  test("It should response with the setting for the userId", async () => {
    let userId = "userIDTest";
    const response = await request(app)
      .get(`/api/getUserSettings/${userId}`)
    expect(response.statusCode).toBe(200);
    expect(response.text).toContain([userId]);
  });
});

describe("Test the get user settings", () => {
  test("It should response with an error when user id is empty in the request", async () => {
    const response = await request(app)
      .get("/api/getUserSettings")
      .send();
    expect(response.statusCode).toBe(404);
    expect(response.text).toContain([]);
  });
});

describe("Test the set user settings", () => {
  test("It should update user settings and return updated values", async () => {
    // Arange
    let userId = guid();
    const saveResponse = await request(app)
      .post("/api/setDefaultValuesForUserSettings")
      .send({ userId: userId });
    var userSettings = saveResponse.body;
    userSettings.selectSetting = "sectedSomethingNew";

    // Act
    const response = await request(app)
      .post("/api/updateUserSettings")
      .send({ userSettings: userSettings });

    // Assert
    expect(response.statusCode).toBe(200);
    expect(response.text).toContain("sectedSomethingNew");
  });
});

describe("Test the set default values for user settings", () => {
  test("It should response with the userId if there is no validation error", async () => {
    let userId = "userIDTest";
    const response = await request(app)
      .post("/api/setDefaultValuesForUserSettings")
      .send({ userId: userId });
    expect(response.statusCode).toBe(200);
    expect(response.text).toContain(userId);
  });
});

describe("Test the set default values for user settings", () => {
  test("It should response with an error when user id is empty in the request", async () => {
    const response = await request(app)
      .post("/api/setDefaultValuesForUserSettings")
      .send();
    expect(response.statusCode).toBe(500);
    expect(response.text).toContain("UserSettings validation failed: userId");
  });
});
