const app = require('./app')
const http = require('http');

const server = http.createServer(app)
server.listen(app.get('port'), () => {
    // tslint:disable-next-line:no-console
    console.log('express listening on port ' + app.get('port'))
})
