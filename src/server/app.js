const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const UserSettings = require("./userSettingsModel");
var mongoClient = require("mongodb").MongoClient;
var Schema = mongoose.Schema;

const azureConnectionString =
  "mongodb://mongodbazure:lGvordhH7Rg7nc0IaLvT2WB7WWecBxzvNySUpUFnbpjyeIgJDI3T9lCDbznG0DFIiYg9T3xpqEumUK0CvYtavw%3D%3D@mongodbazure.documents.azure.com:10255/?ssl=true";
// mongoClient.connect("mongodb://mongodbazure:lGvordhH7Rg7nc0IaLvT2WB7WWecBxzvNySUpUFnbpjyeIgJDI3T9lCDbznG0DFIiYg9T3xpqEumUK0CvYtavw%3D%3D@mongodbazure.documents.azure.com:10255/?ssl=true", function (err, client) {
//   client.close();
// });
mongoose.connect(azureConnectionString);

// Move to separte file?

const app = express();
app.set("port", 8080);

app.use(express.static(path.join(__dirname, "..", "build", "static")));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post("/api/setDefaultValuesForUserSettings", (req, res) => {
  // TODO validate req.body as userId
  // TODO userId should be unique, if already exist do nothing there
  var defaultUserSettings = new UserSettings({
    backgroundColor: "white",
    selectSetting: "selectedSomething",
    showSomethingElse: false,
    showSometing: true,
    userId: req.body.userId
  });
  defaultUserSettings.save(function(error) {
    if (error) {
      res.status(500).send(error);
    } else {
      res.send(defaultUserSettings);
    }
  });
});

app.get('/api/getUserSettings/:userId', (req, res) => {
  console.log(req.param('userId'));
  var query = UserSettings.find({ userId: req.param('userId') });
  query.exec(function(error, doc) {
    res.send(doc);
  });
});

app.post("/api/updateUserSettings", (req, res) => {
  UserSettings.findOne({ userId: req.body.userSettings.userId }, function(
    error,
    currentSettings
  ) {
    if (error) {
      res.status(500).send(error);
    } else {
      var newSettings = req.body.userSettings;
      currentSettings.backgroundColor = newSettings.backgroundColor;
      currentSettings.showSometing = newSettings.showSometing;
      currentSettings.showSomethingElse = newSettings.showSomethingElse;
      currentSettings.selectSetting = newSettings.selectSetting;

      currentSettings.save(function(error, settings) {
        if (error) {
          res.status(500).send(error);
        }
        res.send(settings);
      });
    }
  });
});

module.exports = app;
