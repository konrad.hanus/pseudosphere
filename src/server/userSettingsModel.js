var mongoose = require('mongoose');
 
var userSettingsSchema = mongoose.Schema({
    userId: { type: String, required: true },
    backgroundColor: { type: String, required: true },
    showSometing: { type: Boolean, required: true },
    showSomethingElse: { type: Boolean, required: true },
    selectSetting: { type: String, required: true }
});
 
var UserSettings = mongoose.model('UserSettings', userSettingsSchema);
 
module.exports = UserSettings;