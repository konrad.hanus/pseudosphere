import { UserSettingsState } from "./landingPage/containers/UserSettings/types";

export interface Product{
  id: number;
  name: string;
  price: number;
}

export interface SelectedProduct
{
    product: Product;
}

export interface SearchFieldInput
{
    value: any;
}


export interface ApplicationState {
  allProducts: Product[],
  searchFieldInput: SearchFieldInput,
  selectedProduct: SelectedProduct
  userSettings:  UserSettingsState
}
